﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key : MonoBehaviour
{
    [SerializeField] private KeyType keyType;
    public enum KeyType
    {
        // Provided default key colours to represent different key types if we needed them.
        Red,
        Blue,
        Yellow
    }

    public KeyType GetKeyType()
    {
        return keyType;
    }
}
