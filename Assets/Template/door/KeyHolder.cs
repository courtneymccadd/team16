﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyHolder : MonoBehaviour
{
    // This script links the keys that have been picked up to the player/main character. 
    // (The Key Holder is the player/main character).

    private List<Key.KeyType> keyList;

    private void Awake()
    {
        keyList = new List<Key.KeyType>();
    }

    public void AddKey(Key.KeyType keyType)
    {
        // Adds key to the list.
        Debug.Log("Added Key: " + keyType);
        keyList.Add(keyType);
    }

    public void RemoveKey(Key.KeyType keyType)
    {
        // Removes key from the list.
        keyList.Remove(keyType);
    }

    public bool ContainsKey(Key.KeyType keyType)
    {
        // Checks if list contains key.
        return keyList.Contains(keyType);
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        // If collide with key, picks up the key.
        Key key = collider.GetComponent<Key>();
        if (key != null)
        {
            AddKey(key.GetKeyType());
            // Destroy the key game object once key is added to list.
            Destroy(key.gameObject);
        }

        // If collides with a door, checks that we have the right key for the door then removes it.
        KeyDoor keyDoor = collider.GetComponent<KeyDoor>();
        if (ContainsKey(keyDoor.GetKeyType()))
        {
            // Currently holding key to open this door.
            keyDoor.OpenDoor();
            RemoveKey(keyDoor.GetKeyType());
        }
    }
}
