﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyDoor : MonoBehaviour
{
    // Specify what key opens the door.
    [SerializeField] private Key.KeyType keyType;

    public Key.KeyType GetKeyType()
    {
        return keyType;
    }

    // Open the door.
    // NOTE: This will probably need to be changed to an animation. Created this as a quick and easy alternative 
    // for the prototype.
    public void OpenDoor()
    {
        gameObject.SetActive(false);
    }
}
