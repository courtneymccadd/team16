﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    public float moveX;
    public float moveY;

    public Animator anim;

    public LayerMask environment;

    void Update()
    {
      moveX = 0;
      if (Input.GetKey(KeyCode.A))
      {
        moveX -= 10 * Time.deltaTime;
      }   
      if (Input.GetKey(KeyCode.D))
      {
         moveX += 10 * Time.deltaTime;
      }

      RaycastHit2D hitX;
      hitX = Physics2D.BoxCast(transform.position, Vector2.one, 0, Vector2.right, moveX, environment);
      if (hitX)
      {
          moveX = Mathf.Sign(moveX) * (hitX.distance - 0.01f);
      }

      transform.position += new Vector3(moveX, 0, 0);
        
      moveY -= 2.5f * Time.deltaTime;

        RaycastHit2D hitG;
        hitG = Physics2D.BoxCast(transform.position, Vector2.one, 0, Vector2.up, -0.01f, environment);
        if (hitG)
        {
            if (Input.GetKeyDown(KeyCode.W))
            {
                moveY = 0.3f;
            }
        }


      RaycastHit2D hitY;
      hitY = Physics2D.BoxCast(transform.position, Vector2.one, 0, Vector2.up, moveY, environment);
      if (hitY)
      {
          moveY = Mathf.Sign(moveY) * (hitY.distance - 0.01f);
      }

        transform.position += new Vector3(0, moveY, 0);

        if (Mathf.Abs(moveX) > 0.01f)
        {
            anim.SetBool("IsRunning", true);
        }
        else
        {
            anim.SetBool("IsRunning", false);
        }
      
    }
}
