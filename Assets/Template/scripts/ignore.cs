﻿using UnityEngine;
using System.Collections;

public class ignore : MonoBehaviour
{
    public Transform bulletPrefab;

    void Start()
    {
        Transform box = Instantiate(bulletPrefab) as Transform;
        Physics.IgnoreCollision(box.GetComponent<Collider>(), GetComponent<Collider>());
    }
}
