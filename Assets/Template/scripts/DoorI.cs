﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface DoorI 
{

   void OpenDoor();
   void CloseDoor();
   void ToggleDoor();

}

