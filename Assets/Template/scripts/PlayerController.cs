﻿using System.Collections; 
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Rigidbody2D PM;
    private Collider2D Coll;
    private Animator Anim;

      public float moveX;
    public float moveY;

    public float speed, JumpForce;
    public Transform GroundCheck;
    public LayerMask Environment;

    public bool isGround, isJump;

    bool JumpPressed;
    int JumpCount;

    public LayerMask jumpPadLayerMask;
    public float jumpPadCastDistance = 4.0f;

    public float JumpSpeed = 10;
    void Start()
    {
        PM = GetComponent<Rigidbody2D>();
        Coll = GetComponent<Collider2D>();
        Anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Jump") && JumpCount > 0)
        {
            JumpPressed = true;
        }

     
    RaycastHit2D hit;
        if(Physics2D.Raycast(transform.position, Vector3.down, jumpPadCastDistance, jumpPadLayerMask))
        {
            moveY = JumpSpeed *2.1f;
        }
     RaycastHit2D hitX;
      hitX = Physics2D.BoxCast(transform.position, Vector2.one, 0, Vector2.right, moveX, Environment);
      if (hitX)
      {
          moveX = Mathf.Sign(moveX) * (hitX.distance - 0.01f);
      }
    
    
    }



    

    private void FixedUpdate() 
    {
        isGround = Physics2D.OverlapCircle(GroundCheck.position, 0.1f, Environment);

        GroundMovement();

        Jump();

        SwitchAnim();

    }
    
    void GroundMovement()
    {
        float Horizontalmove = Input.GetAxis("Horizontal");
        float Facedirection = Input.GetAxisRaw("Horizontal");
        PM.velocity = new Vector2(Horizontalmove * speed, PM.velocity.y);

        if (Facedirection != 0)
        {
            transform.localScale = new Vector3(Facedirection, 1, 1);
        }
    }

    void Jump()
    {
        if(isGround)
        {
            JumpCount = 2;
            isJump = false;
        }
        if (JumpPressed && isGround)
        {
            isJump = true;
            PM.velocity = new Vector2(PM.velocity.x, JumpForce);
            JumpCount--;
            JumpPressed = false;
        }
        else if (JumpPressed && JumpCount > 0 && isJump)
        {
            PM.velocity = new Vector2(PM.velocity.x, JumpForce);
            JumpCount--;
            JumpPressed = false;
        }
    }

    void SwitchAnim()
    {
        Anim.SetFloat("running", Mathf.Abs(PM.velocity.x));

        if (isGround)
        {
            Anim.SetBool("falling", false);
        }
        else if (!isGround && PM.velocity.y > 0)
        {
            Anim.SetBool("jumping", true);
        }
        else if (PM.velocity.y < 0)
        {
            Anim.SetBool("jumping", false);
            Anim.SetBool("falling", true);
        }
    }
}
