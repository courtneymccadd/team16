using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pressureplate : MonoBehaviour
{

    [SerializeField] public GameObject doorGameObject;
    private DoorI door1;
    private float timer;

    private void Awake()
    {
        door1 = doorGameObject.GetComponent<DoorI>();
    }

    private void Update()
    {
        if (timer > 0f)
        {
            timer -= Time.deltaTime;
            if (timer <= 0f)
            { 
                door1.CloseDoor();
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collider)              // In order for the box to trigger the pressure plate, 
    {                                                               // it needs to be tagged with "Box" - keep this in mind when 
        if (collider.tag == "Player" || collider.tag == "Box")      // adding boxes to the levels. 
        {
            door1.OpenDoor();
        }

    }
  
    private void OnTriggerStay2D(Collider2D collider)               // I'm not sure what was causing the pressure plate to not
    {                                                               // work, all I did was rewrite this section and it started 
        if (collider.tag == "Player" || collider.tag == "Box")      // working.
        {
            timer = 1f;
        }
    }

}